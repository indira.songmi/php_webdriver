<?php

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

require_once(__DIR__ . '/vendor/autoload.php');

$host = 'http://localhost:4444/wd/hub'; // this is the default
$USE_FIREFOX = false; // if false, will use chrome.

if ($USE_FIREFOX)
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::firefox()
    );
}
else
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::chrome()
    );
}

$driver->get("http://www.google.com");

# enter text into the search field
$driver->findElement(\Facebook\WebDriver\WebDriverBy::className('gLFyf'))->click();
sleep(1);
$driver->findElement(Facebook\WebDriver\WebDriverBy::className('gLFyf'))->sendKeys('hello');
sleep(1);
//wait
//$driver->wait()->until(
//    WebDriverExpectedCondition::titleIs('My Page')
//);

//popup prompt
$driver->executeScript("return prompt('Enter your input: ')");
sleep(2);
$driver->switchTo()->alert()->dismiss();



//$driver->findElement(Facebook\WebDriver\WebDriverBy::name('btnK'))->click();
$element = $driver->findElement(WebDriverBy::name('btnK'));
$driver->getMouse()->mouseMove( $element->getCoordinates() );

