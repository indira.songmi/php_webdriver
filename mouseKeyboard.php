<?php

use Facebook\WebDriver\WebDriverBy;

require_once(__DIR__ . '/vendor/autoload.php');

$host = 'http://localhost:4444/wd/hub'; // this is the default
$USE_FIREFOX = false; // if false, will use chrome.

if ($USE_FIREFOX)
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::firefox()
    );
}
else
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::chrome()
    );
}

$driver->get("https://www.facebook.com");
$login_button = $driver->findElement(WebDriverBy::id('loginbutton'));
$driver->getMouse()->mouseMove($login_button->getCoordinates());
//$driver->getMouse()->click();