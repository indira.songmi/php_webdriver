<?php

use Facebook\WebDriver\Remote\LocalFileDetector;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;

require_once(__DIR__ . '/vendor/autoload.php');

$host = 'http://localhost:4444/wd/hub'; // this is the default
$USE_FIREFOX = false; // if false, will use chrome.

if ($USE_FIREFOX)
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::firefox()
    );
}
else {
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::chrome()
    );
}


$driver->get("http://cms.loc:8080/login");


# screenshots
function TakeScreenshot($element=null,$driver) {
    // Change the Path to your own settings
    $screenshot = __DIR__ . '/screenshot/' .rand().'.png';;

    if( ! (bool) $element) {
        return $screenshot;
    }

    $element_screenshot = __DIR__ . '/screenshot/' .rand().'.png';// Change the path here as well

    $element_width = $element->getSize()->getWidth();
    $element_height = $element->getSize()->getHeight();

    $element_src_x = $element->getLocationOnScreenOnceScrolledIntoView()->getX();
    $element_src_y = $element->getLocationOnScreenOnceScrolledIntoView()->getY();

    // Change the driver instance
    $driver->takeScreenshot($screenshot);
    if(! file_exists($screenshot)) {
        throw new Exception('Could not save screenshot');
    }

    // Create image instances
    $src = imagecreatefrompng($screenshot);
    $dest = imagecreatetruecolor($element_width, $element_height);

    // Copy
    imagecopy($dest, $src, 0, 0, (int) ceil($element_src_x), (int) ceil($element_src_y), (int) ceil($element_width), (int) ceil($element_height));

    imagepng($dest, $element_screenshot);

    // unlink($screenshot); // unlink function might be restricted in mac os x.

    if( ! file_exists($element_screenshot)) {
        throw new Exception('Could not save element screenshot');
    }

    return $element_screenshot;
}

$screenshot_of_element = TakeScreenshot($element=$driver->findElement(WebDriverBy::id('email')),$driver);