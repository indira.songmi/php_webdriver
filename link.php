<?php

use Facebook\WebDriver\WebDriverBy;

require_once(__DIR__ . '/vendor/autoload.php');

$host = 'http://localhost:4444/wd/hub'; // this is the default
$USE_FIREFOX = false; // if false, will use chrome.

if ($USE_FIREFOX)
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::firefox()
    );
}
else
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::chrome()
    );
}

$driver->get("http://cms.loc:8080");


//clicking image link
$driver->findElement(WebDriverBy::cssSelector("a[title=\"recent\"]"))->click(); //<a title = "recent">img </a>
sleep(4);

//clicking text link

$driver->findElement(WebDriverBy::linkText('Home'))->click();


