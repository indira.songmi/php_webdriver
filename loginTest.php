<?php

use Facebook\WebDriver\WebDriverBy;

require_once(__DIR__ . '/vendor/autoload.php');

$host = 'http://localhost:4444/wd/hub'; // this is the default
$USE_FIREFOX = false; // if false, will use chrome.

if ($USE_FIREFOX)
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::firefox()
    );
}
else
{
    $driver = Facebook\WebDriver\Remote\RemoteWebDriver::create(
        $host,
        Facebook\WebDriver\Remote\DesiredCapabilities::chrome()
    );
}

$driver->get("http://cms.loc:8080/login");

# enter text into the search field

$driver->findElement(Facebook\WebDriver\WebDriverBy::id('email'))->sendKeys('indira@gmail.com');
//$driver->findElement(Facebook\WebDriver\WebDriverBy::id('email'))->clear();

$driver->findElement(Facebook\WebDriver\WebDriverBy::id('password'))->sendKeys('indira');
sleep(1);

# Click the search button

$driver->findElement(Facebook\WebDriver\WebDriverBy::name('loginTest'))->click();
